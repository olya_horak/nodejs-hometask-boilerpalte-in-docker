const { fighter } = require('../models/fighter');
const { FighterRepository } = require('../repositories/fighterRepository');

const createFighterValid = (req, res, next) => {
    // Implement validatior for fighter entity during creation
    const reqFighter = req.body;
    const { name, health, power, defense } = reqFighter;

    const required = ["name", "power", "defense"];

    required.map(field => {
        if (!reqFighter[field]) {
            res.err = `${field} is required`
        }
    })


    const isNumber = ["power", "defense"];

    isNumber.map(field => {
        const num = reqFighter[field];
        if (isNaN(Number(num))) {
            res.err = `${field} should be a number`
        }
    })

    if (!res.err) {

        if (health < 80 || health > 120) {
            res.err = "Health can be between 80 and 120";
        }
        if (power < 1 || power > 100) {
            res.err = "Power should be between 1 and 100";
        }
        if (defense < 1 || defense > 10) {
            res.err = "Defense should be between 1 and 10";
        }

        const fighterNameInUse = FighterRepository.exists(
            (dbFighter) => {
                return dbFighter.name.toUpperCase() === reqFighter.name.toUpperCase();
            }
        );
        if (fighterNameInUse) {
            res.err = "Fighter with such name already exists";
        }

    }
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const reqFighter = req.body;
    const { name, health, power, defense } = reqFighter;

    const requiredSome = ["name", "power", "defense"];

    const hasSome = requiredSome.some(field => {
        return reqFighter[field]
    });

    if (!hasSome) {
        res.err = "Nothing to be updated";
    }

    const isNumber = ["power", "defense"];

    isNumber.map(field => {
        const num = reqFighter[field];
        if (num !== undefined && isNaN(Number(num))) {
            res.err = `${field} should be a number`
        }
    })

    if (!res.err) {

        if (health < 80 || health > 120) {
            res.err = "Health can be between 80 and 120";
        }
        if (power < 1 || power > 100) {
            res.err = "Power should be between 1 and 100";
        }
        if (defense < 1 || defense > 10) {
            res.err = "Defense should be between 1 and 10";
        }

        if (reqFighter.name !== undefined) {
            const fighterNameInUse = FighterRepository.exists(
                (dbFighter) => {

                    return dbFighter.name.toUpperCase() === reqFighter.name.toUpperCase();
                }, req.params.id
            );
            if (fighterNameInUse) {
                res.err = "Fighter with such name already exists";
            }
        }

    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;