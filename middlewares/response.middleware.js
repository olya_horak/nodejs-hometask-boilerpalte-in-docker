const responseMiddleware = (req, res, next) => {
    // Implement middleware that returns result of the query
  
    if (res.err) {
        res.statusCode = 400;
        res.send({
            error: true,
            message: res.err.toString()
        })
    }
    else if (!res.data){
        res.statusCode = 404;
        res.send({
            error: true,
            message: "Not found"
        })
    }
    else res.send(res.data)
    next();
}

exports.responseMiddleware = responseMiddleware;