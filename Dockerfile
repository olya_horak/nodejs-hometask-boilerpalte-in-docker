FROM node:6-alpine

COPY client ./client
COPY config ./config
COPY middlewares ./middlewares
COPY models ./models
COPY repositories ./repositories
COPY routes ./routes
COPY services ./services
COPY index.js ./
COPY package*.json ./
COPY build-start.sh ./
RUN npm install

CMD ["node", "index.js"]
EXPOSE $PORT