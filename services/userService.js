const { UserRepository } = require('../repositories/userRepository');

class UserService {

    //Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    getAll(){
        const items = UserRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    create(user) {
        const item = UserRepository.create(user);
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = UserRepository.update(id, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const item = UserRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }


}

module.exports = new UserService();