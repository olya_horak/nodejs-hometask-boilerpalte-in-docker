const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // Implement methods to work with fighters
    create(fighter) {
        fighter.health = fighter.health || 100;
        const item = FighterRepository.create(fighter);
        if(!item) {
            return null;
        }
        return item;
    }

    getAll(){
        const items = FighterRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }

    delete(id) {
        const item = FighterRepository.delete(id);
        if(!item) {
            return null;
        }
        return item;
    }

    update(id, dataToUpdate) {
        const item = FighterRepository.update(id, dataToUpdate);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();